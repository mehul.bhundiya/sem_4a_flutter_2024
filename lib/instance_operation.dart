class A {
  int a = 10;

  static A? abc;

  static A? getInstance() {
    return abc ??= A();
  }
}

void main() {
  A a = A.getInstance()!;
  A a1 = A.getInstance()!;

  print('VALUE OF A : ${a.a}');
  print('VALUE OF A1 : ${a1.a}');

  a.a = 20;
  a1.a = 50;

  print('VALUE OF A : ${a.a}');
  print('VALUE OF A1 : ${a1.a}');
}
