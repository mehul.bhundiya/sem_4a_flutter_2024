import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageProcessPage extends StatefulWidget {
  @override
  State<ImageProcessPage> createState() => _ImageProcessPageState();
}

class _ImageProcessPageState extends State<ImageProcessPage> {
  XFile? image;

  List<XFile> images = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            ElevatedButton(
              onPressed: () {
                getImageFromGallery();
              },
              child: Text('Capture Image'),
            ),
            images.isNotEmpty
                ? Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return Image.file(
                          File(images[index].path),
                          height: 300,
                          width: 300,
                        );
                      },
                      itemCount: images.length,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  Future<void> getImageFromGallery() async {
    final ImagePicker picker = ImagePicker();
    XFile? file = await picker.pickImage(source: ImageSource.camera);
    images.add(file!);
    setState(() {});
  }
}
