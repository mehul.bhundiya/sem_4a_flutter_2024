import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:project_a/UserListMode.dart';
import 'package:project_a/user_entry_page.dart';

class ApiUserListPage extends StatefulWidget {
  const ApiUserListPage({super.key});

  @override
  State<ApiUserListPage> createState() => _ApiUserListPageState();
}

class _ApiUserListPageState extends State<ApiUserListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('User List'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return UserEntryPage();
                  },
                ),
              );
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
      body: FutureBuilder(
        future: getUserListFromApi(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            if (snapshot.data!.isNotEmpty) {
              return ListView.builder(
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return UserEntryPage(
                            name: snapshot.data![index].name.toString(),
                            userId: snapshot.data![index].id,
                          );
                        },
                      ));
                    },
                    title: Text(
                      snapshot.data![index].name.toString(),
                    ),
                    trailing: IconButton(
                        onPressed: () async {
                          await deleteUserFromAPI(
                              userID: snapshot.data![index].id);
                          setState(() {});
                        },
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red,
                        )),
                  );
                },
                itemCount: snapshot.data!.length,
              );
            } else {
              return Center(child: Text('No Users Found'));
            }
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }

  Future<List<UserListModel>> getUserListFromApi() async {
    http.Response response = await http
        .get(Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user'));
    List<dynamic> userList = jsonDecode(response.body);
    List<UserListModel> userListModel = [];
    for (int i = 0; i < userList.length; i++) {
      userListModel.add(
        UserListModel(
          id: userList[i]['id'],
          name: userList[i]['name'],
          city: userList[i]['City'],
        ),
      );
    }
    return userListModel;
  }

  Future<dynamic> deleteUserFromAPI({required userID}) async {
    return await http.delete(Uri.parse(
        'https://638029948efcfcedacfe0228.mockapi.io/api/user/$userID'));
  }
}
