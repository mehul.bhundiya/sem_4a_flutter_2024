import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_a/my_database.dart';
import 'package:http/http.dart' as http;

class UserEntryPage extends StatelessWidget {
  String? name;
  String? userId;

  UserEntryPage({this.name, this.userId});

  TextEditingController nameController = TextEditingController();
  TextEditingController mobileNoController = TextEditingController();

  GlobalKey<FormState> validationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    nameController.text = name.toString();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text(
          'User Detail',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: validationKey,
          child: Column(
            children: [
              TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'ENTER NAME OF USER';
                  }
                },
                controller: nameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      20,
                    ),
                  ),
                  hintText: 'Enter User Name',
                  labelText: 'Enter User Name',
                  suffix: Icon(
                    Icons.clear,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'ENTER Mobile Number';
                  }
                  if (value.length != 10) {
                    return 'ENTER VALID MOBILE NUMBER';
                  }
                },
                controller: mobileNoController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      20,
                    ),
                  ),
                  hintText: 'Enter Mobile Number',
                  labelText: 'Enter Mobile Number',
                  suffix: Icon(
                    Icons.clear,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: () async {
                  if (validationKey.currentState!.validate()) {
                    if (userId == null) {
                      insertDetailUsingApi();
                    } else {
                      updateDetailUsingApi(userId);
                    }
                  }
                },
                child: Text(
                  'SUBMIT',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> insertDetailUsingApi() async {
    Map<dynamic, dynamic> map = {};
    map['name'] = nameController.text.toString();
    http.Response response = await http.post(
      Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user'),
      body: map,
    );
    print('DATA INSERTED ::: ${response.body}');
  }

  Future<void> updateDetailUsingApi(id) async {
    Map<dynamic, dynamic> map = {};
    map['name'] = nameController.text.toString();
    http.Response response = await http.put(
      Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user/$id'),
      body: map,
    );
    print('DATA UPDATED ::: ${response.body}');
  }

  void displayAlert(context, title, message, onPopAlert) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(title),
          content: Text(message),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  onPopAlert();
                },
                child: Text('Ok'))
          ],
        );
      },
    );
  }

  void parse() {
    Map<dynamic, dynamic> data = jsonDecode(
        "{ 'User': { 'Phone': { 'Home': 1234567890, 'Office': '1234567890', 1: 'Data' }, 'Address': [ 'AVENUE', 'Regency', 'Recidensy' ] } }");
    print('HOME NUMBER :: ${data['User']['Address'][1]}');
  }
}
