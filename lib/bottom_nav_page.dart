import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_a/pre_login_page.dart';
import 'package:project_a/screen_division_page.dart';
import 'package:project_a/splash_screen.dart';
import 'package:project_a/user_entry_page.dart';
import 'package:project_a/user_list_page.dart';

class BottomNavPage extends StatefulWidget {
  @override
  State<BottomNavPage> createState() => _BottomNavPageState();
}

class _BottomNavPageState extends State<BottomNavPage> {
  int indexValue = 1;

  List<Widget> pages = [
    PreLoginPage(),
    ScreenDivisonPage(),
    // SplashScreenPage(),
    // UserListPage(),
    // UserEntryPage(),
    // Text(
    //   'VALUE OF WIDGET',
    //   style: TextStyle(
    //     color: Colors.red,
    //     fontWeight: FontWeight.bold,
    //     fontSize: 30,
    //   ),
    // ),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: pages.length,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text('TAB CONTROLLER'),
          bottom: TabBar(
            indicatorWeight: 10,
            indicatorSize: TabBarIndicatorSize.tab,
            onTap: (value) {
              setState(() {
                indexValue = value;
              });
            },
            tabs: const [
              Column(
                children: [
                  Icon(
                    Icons.mobile_screen_share,
                  ),
                  Text('Mobile')
                ],
              ),
              Column(
                children: [
                  Icon(
                    Icons.computer,
                  ),
                  Text('Personal Computer')
                ],
              ),
            ],
          ),
        ),
        drawer: Drawer(
          child: Column(
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.green,
                ),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        CircleAvatar(
                          // child: Image.asset(
                          //   'assets/images/bg_1.jpg',
                          // ),
                          backgroundImage: AssetImage(
                            'assets/images/bg_1.jpg',
                          ),
                          minRadius: 40,
                        ),
                        Text(
                          'Mehul Bhundiya',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          'mehul.bhundiya@darshan.ac.in',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              getDrawerMenu(
                callBackFunction: () {
                  print(':::::::::HOME:::::::::::');
                },
                title: 'Home',
                leadingIcon: Icons.home,
                trailingIcon: Icons.arrow_forward_ios,
              ),
              Divider(color: Colors.grey.shade300),
              getDrawerMenu(
                callBackFunction: () {
                  print(':::::::::Pre Login Page:::::::::::');
                },
                title: 'Pre Login Page',
                leadingIcon: Icons.login,
                // trailingIcon: Icons.arrow_forward_ios,
              ),
              Divider(color: Colors.grey.shade300),
              getDrawerMenu(
                callBackFunction: () {
                  showCupertinoDialog(
                    context: context,
                    builder: (context) {
                      return CupertinoAlertDialog(
                        title: Text(
                          'Alert!',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 24,
                          ),
                        ),
                        content: Text(
                          'Are you sure want to logout?',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 21,
                          ),
                        ),
                        actions: [
                          TextButton(
                            onPressed: () {},
                            child: Text(
                              'Yes',
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                          TextButton(
                            onPressed: () {},
                            child: Text(
                              'No',
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  );
                },
                title: 'Logout',
                leadingIcon: Icons.logout,
                trailingIcon: null,
              ),
            ],
          ),
        ),
        body: TabBarView(children: pages),
        // pages[indexValue],
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.list,
                ),
                label: 'LIST'),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.grid_3x3,
              ),
              label: 'GRID',
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.list,
                ),
                label: 'LIST'),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.grid_3x3,
              ),
              label: 'GRID',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.grid_3x3,
              ),
              label: 'GRID',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.grid_3x3,
              ),
              label: 'GRID',
            ),
          ],
          selectedItemColor: Colors.red,
          unselectedItemColor: Colors.yellow,
          showUnselectedLabels: false,
          showSelectedLabels: false,
          currentIndex: indexValue,
          onTap: (value) {
            setState(() {
              indexValue = value;
              print('VALUE OF INDEX ::: $indexValue');
            });
          },
        ),
      ),
    );
  }

  Widget getDrawerMenu({
    required String title,
    required GestureTapCallback callBackFunction,
    IconData? leadingIcon,
    IconData? trailingIcon = Icons.arrow_forward_ios,
  }) {
    return ListTile(
      onTap: callBackFunction,
      title: Text(
        title,
      ),
      leading: Icon(
        leadingIcon,
        color: Colors.grey.shade500,
        size: 20,
      ),
      trailing: Icon(
        trailingIcon,
        color: Colors.grey.shade300,
        size: 18,
      ),
    );
  }
}
