import 'package:sqflite/sqflite.dart';

class MyDatabase {
  Future<Database> initDatabase() async {
    var db = await openDatabase('matrimony.db', onCreate: (db, version) {
      db.execute(
          'CREATE TABLE TblUser(UserId INTEGER PRIMARY KEY AUTOINCREMENT, UserName TEXT)');
    }, onUpgrade: (db, oldVersion, newVersion) {
      // if(newVersion > oldVersion){
      //   db.execute('ALTER TABLE TblUser()');
      // }
    }, version: 1);
    return db;
  }

  Future<int> insertDetailInTblUser(dynamic name) async {
    Database db = await initDatabase();
    Map<String, dynamic> values = {};
    values['UserName'] = name;
    int primaryKey = await db.insert('TblUser', values);
    return primaryKey;
  }

  Future<int> updateDetailInTblUserByUserId(dynamic name, int userId) async {
    Database db = await initDatabase();
    Map<String, dynamic> value = {};
    value['UserName'] = name;
    int userID = await db
        .update('TblUser', value, where: 'UserId = ?', whereArgs: [userId]);
    return userID;
  }

  Future<List<Map<String, dynamic>>> getUserListFromUserTable() async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> userList = await db.query('TblUser');
    return userList;
  }

  Future<int> deleteUserByUserId(int userID) async {
    Database db = await initDatabase();
    int userId =
        await db.delete('TblUser', where: 'UserId = ?', whereArgs: [userID]);
    return userId;
  }
}
