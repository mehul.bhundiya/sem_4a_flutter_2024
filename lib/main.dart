import 'package:flutter/material.dart';
import 'package:project_a/Image_process_page.dart';
import 'package:project_a/api_user_list_page.dart';
import 'package:project_a/my_database.dart';
import 'package:project_a/pre_login_page.dart';
import 'package:project_a/screen_division_page.dart';
import 'package:project_a/splash_screen.dart';
import 'package:project_a/user_entry_page.dart';
import 'package:project_a/user_list_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bottom_nav_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  // SharedPreferences pref = SharedPreferences.getInstance();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.deepPurple,
          primary: Colors.yellow,
        ),
        primaryColor: Colors.cyan,
        errorColor: Colors.blue,
        useMaterial3: true,
      ),
      home: ImageProcessPage(),

      // FutureBuilder(
      //   future: MyDatabase().initDatabase(),
      //   builder: (context, snapshot) {
      //     if (snapshot.hasData && snapshot.data != null) {
      //       return FutureBuilder<SharedPreferences>(
      //         future: SharedPreferences.getInstance(),
      //         builder: (context, snapshot1) {
      //           if (snapshot1.connectionState == ConnectionState.done) {
      //             if (snapshot1.hasError) {
      //               return Text('ERROR ${snapshot1.error}');
      //             } else if (snapshot1.hasData) {
      //               if (snapshot1.data!.getBool('IsButtonClicked') != null &&
      //                   snapshot1.data!.getBool('IsButtonClicked')!) {
      //                 return ;
      //               } else {
      //                 return SplashScreenPage();
      //               }
      //             }
      //           }
      //           return CircularProgressIndicator();
      //         },
      //       );
      //     } else {
      //       return CircularProgressIndicator();
      //     }
      //   },
      // )

      // BottomNavPage(),
    );
  }

  Future<bool> fetchUserOrder() async {
    return await Future.delayed(
      Duration(seconds: 2),
      () {
        return true;
      },
    );
  }
}
