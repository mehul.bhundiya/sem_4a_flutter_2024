import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_a/my_database.dart';
import 'package:project_a/splash_screen.dart';
import 'package:project_a/user_entry_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  TextEditingController sharedController = TextEditingController();
  List<Map<String, dynamic>> userList = [];
  List<Map<String, dynamic>> filterList = [];
  bool isGetData = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          'User List',
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () async {
                SharedPreferences pref = await SharedPreferences.getInstance();
                await pref.remove('IsButtonClicked');
                // pref.getString('key')
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return UserEntryPage();
                    },
                  ),
                ).then((value) {
                  if (value) {
                    userList.clear();
                    filterList.clear();
                    isGetData = true;
                    Future.delayed(
                      Duration(microseconds: 5),
                      () {
                        setState(() {});
                      },
                    );
                  }
                });
              },
              child: Icon(
                Icons.add,
              ),
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          TextFormField(
            onChanged: (value) {
              // filterList.clear();
              // for (int i = 0; i < userList.length; i++) {
              //   if (userList[i]['UserName']
              //       .toString()
              //       .toLowerCase()
              //       .contains(value.toLowerCase())) {
              //     filterList.add(userList[i]);
              //   }
              // }
              // setState(() {});
            },
          ),
          Expanded(
            child: FutureBuilder(
              future: MyDatabase().getUserListFromUserTable(),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data != null) {
                  if (snapshot.data!.isNotEmpty) {
                    userList.clear();
                    filterList.clear();
                    userList.addAll(snapshot.data!);
                    filterList.addAll(snapshot.data!);
                    isGetData = false;
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        return Card(
                          margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(filterList[index]['UserName']
                                        .toString())),
                                IconButton(
                                  icon: Icon(Icons.edit),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                      builder: (context) {
                                        return UserEntryPage(
                                          name: filterList[index]['UserName']
                                              .toString(),
                                          userId: filterList[index]['UserId'],
                                        );
                                      },
                                    )).then((value) {
                                      if (value != null && value) {
                                        userList.clear();
                                        filterList.clear();
                                        isGetData = true;
                                        Future.delayed(
                                          Duration(microseconds: 5),
                                          () {
                                            setState(() {});
                                          },
                                        );
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    showCupertinoDialog(
                                      context: context,
                                      builder: (context1) {
                                        return CupertinoAlertDialog(
                                          title: Text('Alert!'),
                                          content: Text(
                                              'Are you sure want to delete?'),
                                          actions: [
                                            TextButton(
                                              onPressed: () async {
                                                int userID = await MyDatabase()
                                                    .deleteUserByUserId(
                                                  filterList[index]['UserId'],
                                                );
                                                if (userID > 0) {
                                                  userList.clear();
                                                  filterList.clear();
                                                  isGetData = true;
                                                  setState(() {});
                                                  Navigator.of(context).pop();
                                                }
                                              },
                                              child: Text('Yes'),
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text('No'),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                )
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: filterList.length,
                    );
                  } else {
                    return Center(
                      child: Text(
                        'No User Found',
                      ),
                    );
                  }
                } else {
                  return snapshot.hasData
                      ? Center(
                          child: Text(
                            'No Data Found',
                          ),
                        )
                      : CircularProgressIndicator();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
