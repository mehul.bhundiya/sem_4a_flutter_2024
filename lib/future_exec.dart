Future<void> main() async {
  await execOperation();
  print(':::::::HELLO::::::::');
}

Future<void> execOperation() async {
  print(createOrderMessage());
  await fetchUserOrder();
  print(otherWork());
}

String createOrderMessage() {
  return ':::ORDER MESSAGE DISPATCHED:::';
}

String otherWork() {
  return '::::SAVING AND HAIR CUTTING STARTED::';
}

Future<void> fetchUserOrder() async {
  await Future.delayed(
    Duration(seconds: 5),
    () {
      print(':::YOUR ORDER WITH 5 VADAPAV IS READY:::');
    },
  );
}
