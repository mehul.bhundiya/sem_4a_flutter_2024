class UserModelData{

  UserModelData({userId,userName}){
    this._UserId = userId;
    this._UserName = userName;

  }
  int? _UserId;

  int? get UserId => _UserId;

  set UserId(int? value) {
    _UserId = value;
  }
  String? _UserName;

  String? get UserName => _UserName;

  set UserName(String? value) {
    _UserName = value;
  }
}